var builder = WebApplication.CreateBuilder(args);
var app = builder.Build();

var httpClient = new HttpClient
{
    BaseAddress = new Uri("https://jsonplaceholder.typicode.com"),
};

app.MapGet("/address/{id}", async (int id) => {
    var users = await httpClient.GetFromJsonAsync<List<ExternalUser>>("/users");
    
    if (users == null) {
        return Results.Json(new { Message = "empty users response"}, null, null, 500);
    }

    var user = users.Find(u => u.Id == id);

    if (user == null) {
        return Results.Json(new { Message = "user id not found"}, null, null, 404);
    }

    return Results.Json(new { Id = user.Id, Address = user.AddressLong() });
});

app.Run();

public record Geo(string Lat, string Lng);

public record Address(string Street, string Suite, string City, string Zipcode, Geo Geo);

public record ExternalUser(
    int Id,
    Address Address
) {
    public string AddressLong() {
        return $"{this.Address.City} {this.Address.Zipcode} ({this.Address.Geo.Lat}, {this.Address.Geo.Lng})";
    }
}
