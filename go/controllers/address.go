package controllers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/gin-gonic/gin"

	"example/hello/models"
	"example/hello/utils"
)

type AddressControler struct{}

const JsonPlaceholderUrl = "https://jsonplaceholder.typicode.com/users"

func (a AddressControler) Get(c *gin.Context) {
	if c.Param("id") == "" {
		utils.HandleHttpError(c, http.StatusBadRequest, "missing id")
		return
	}

	resp, err := http.Get(JsonPlaceholderUrl)

	if err != nil {
		utils.HandleHttpError(c, http.StatusInternalServerError, err.Error())
		return
	}

	if resp.StatusCode < 200 || resp.StatusCode >= 299 {
		utils.HandleHttpError(c, http.StatusBadGateway, "unexpected '"+resp.Status+"' response from '"+JsonPlaceholderUrl+"'")
		return
	}

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		utils.HandleHttpError(c, http.StatusInternalServerError, err.Error())
		return
	}

	var result []models.RemoteUser

	if err := json.Unmarshal(body, &result); err != nil {
		utils.HandleHttpError(c, http.StatusInternalServerError, err.Error())
		return
	}

	for i := 0; i < len(result); i++ {
		user := result[i]
		idStr := user.IdString()
		if idStr == c.Param("id") {
			c.JSON(http.StatusOK, gin.H{"id": user.Id, "address": user.AddressLong()})
			return
		}
	}

	utils.HandleHttpError(c, http.StatusNotFound, "not found")
}
