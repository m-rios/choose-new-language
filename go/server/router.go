package server

import (
	"example/hello/controllers"

	"github.com/gin-gonic/gin"
)

func NewRouter() *gin.Engine {
	router := gin.New()
	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	address := new(controllers.AddressControler)

	router.GET("/address/:id", address.Get)

	return router
}
