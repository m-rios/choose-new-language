package utils

import "github.com/gin-gonic/gin"

func HandleHttpError(c *gin.Context, httpStatusCode int, errorMessage string) {
	c.JSON(httpStatusCode, gin.H{"message": errorMessage})
	c.Abort()
}
