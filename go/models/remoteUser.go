package models

import "strconv"

type RemoteUser struct {
	Id       int    `json:"id"`
	Name     string `json:"Name"`
	Username string `json:"username"`
	Email    string `json:"email"`
	Address  struct {
		Street  string `json:"street"`
		Suite   string `json:"suite"`
		City    string `json:"city"`
		Zipcode string `json:"zipcode"`
		Geo     struct {
			Lat string `json:"lat"`
			Lng string `json:"lng"`
		}
	}
	Phone   string `json:"phone"`
	Website string `json:"website"`
	Company struct {
		Name        string `json:"name"`
		CatchPhrase string `json:"catchPhrase"`
		Bs          string `json:"bs"`
	}
}

func (u RemoteUser) IdString() string {
	return strconv.Itoa(u.Id)
}

func (u RemoteUser) AddressLong() string {
	return u.Address.City + " " + u.Address.Zipcode + " (" + u.Address.Geo.Lat + ", " + u.Address.Geo.Lng + ")"
}
